package com.example.ciapp.sample2;

public class ValidateFailedException extends Exception {
    public ValidateFailedException(String msg){
        super(msg);
    }
}
